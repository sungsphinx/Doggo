<!-- Definitions -->
[flathub]: https://flathub.org/apps/app.drey.Doggo
<!-- End Defenitions -->

[![Flathub](https://img.shields.io/flathub/v/app.drey.Doggo?logo=flathub&logoColor=white&label=Flathub)][flathub]
[![Flathub Installs](https://img.shields.io/flathub/downloads/app.drey.Doggo?logo=flathub&logoColor=white&label=Installs)][flathub]
[![Please do not theme this app](https://stopthemingmy.app/badge.svg)](https://stopthemingmy.app)

<img style="vertical-align: middle;" src="data/icons/hicolor/scalable/apps/app.drey.Doggo.svg" width="120" height="120" align="left">

# Doggo
Clicker and chance game
</div>
<br/>


## Screenshots
![Main Window](data/screenshots/screenshot-1.png) | ![Doggo Is Friendly Page](data/screenshots/screenshot-2.png) | ![Doggo Is Unfriendly Page](data/screenshots/screenshot-3.png)
-------------------------------------------- | -------------------------------------- | ----------------------------------------------

## Code of Conduct
When interacting with the project, the [GNOME Code of Conduct](https://conduct.gnome.org/) applies.

## Clone Repo?
1. Make sure git is installed.
2. Run ```git clone https://codeberg.org/SOrg/Doggo.git``` in the directory you want to clone this repo to.

## Install

### Install (Stable Version via Flathub)

[![Download on Flathub](https://dl.flathub.org/assets/badges/flathub-badge-en.svg)][flathub]

### Install (Development Version)

#### You can either:
Use [GNOME Builder](https://apps.gnome.org/Builder) and build the application easily.

[![GNOME Builder](https://apps.gnome.org/icons/scalable/org.gnome.Builder.svg)](https://apps.gnome.org/Builder)

#### Or you can build it manually:

**1.** Install Flatpak (if it isn't installed already, **with [Flathub](https://flathub.org) and [GNOME Nightly](https://wiki.gnome.org/Apps/Nightly)**) and Flatpak Builder.

**2.** Make sure you have ```runtime/org.gnome.Platform/[x86_64/aarch64]/master``` &amp; ```runtime/org.gnome.Sdk/[x86_64/aarch64]/master``` installed, you can install the platform with:

```
flatpak install runtime/org.gnome.Platform/$(uname -m)/master
```

And the SDK with:

```
flatpak install runtime/org.gnome.Platform/$(uname -m)/master
```
### (Choose x86_64 for 64/32bit, aarch64 for arm64)

**3.** Download the [Flatpak Manifest](build-aux/flatpak/app.drey.Doggo.Devel.json) and optionally make a folder to put it in.

**4.** Open a terminal in the folder (if you created one) and choose one of the following to run:

* Install Directly: 
```
flatpak run org.flatpak.Builder --install --force-clean build-dir app.drey.Doggo.Devel.json
```

* Build Bundle: 

```
flatpak run org.flatpak.Builder --repo=repo --force-clean build-dir app.drey.Doggo.Devel.json
```

Then run:

```
flatpak build-bundle repo app.drey.Doggo.Devel.flatpak app.drey.Doggo.Devel
``` 
This will create a bundle (*.flatpak). Read more about single-file bundles [here](https://docs.flatpak.org/en/latest/single-file-bundles.html).

## Credits
[Libadwaita Demo](https://gitlab.gnome.org/GNOME/libadwaita/-/blob/main/demo) - [Debug Information/Troubleshooting for the about dialog](https://gitlab.gnome.org/GNOME/libadwaita/-/blob/main/demo/adw-demo-debug-info.c)
